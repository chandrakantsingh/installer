from subprocess import Popen, PIPE
import wx
import sys
sys.path.append('.')
import subprocess
import time
from WorkerThread import * 


def Messages():
    installer_language = None
    language = open('/etc/locale.conf').read().replace('LANG=', '')
    languages = ['en']
    for lang in languages:
        if language.startswith(lang):
            installer_language = lang
            break

	if installer_language == None:
		installer_language = 'en'
    messages = open('/opt/installer/messages/' + installer_language).read().split('\n')
    return messages

def AddHeadingAndDescription(parent, sizer, heading, description, span2=False):
    heading = wx.StaticText(parent, -1, heading)
    if not span2:
         sizer.Add(heading, (0, 0), (1, 1), wx.EXPAND | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_LEFT | wx.ALL, 10)
    else:
         sizer.Add(heading, (0, 0), (1, 2), wx.EXPAND | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_LEFT | wx.ALL, 10)
    boldFont = wx.Font(14, wx.FONTFAMILY_SCRIPT, wx.NORMAL, wx.BOLD)
    heading.SetFont(boldFont)
    description = wx.StaticText(parent, -1, description)
    description.Wrap(760)
    if not span2:
        sizer.Add(description, (1, 0), (1, 1), wx.EXPAND | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_LEFT | wx.ALL, 10)
    else:
        sizer.Add(description, (1, 0), (1, 2), wx.EXPAND | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_LEFT | wx.ALL, 10)


def runCommand(command, shell=False):
    process = Popen(command, stdout=PIPE, shell=shell)
    (output, err) = process.communicate()
    exit_code = process.wait()
    return output


def getDevices():
    output = runCommand(["/opt/installer/devicelist.sh"], True)
    print output
    lines = output.split("\n")
    devices = list()
    for line in lines:
        if line != '':
            newline = '/dev/' + line.split(' ')[0] + ", Size : " + (line.split(' ')[3].replace(',', '.'))
            devices.append(newline)
    return devices


def getPartitions(device):
    output = runCommand(['sudo', 'partx', '-s', device, '-o', 'NR,START,END,SIZE,TYPE'])
    lines = output.split('\n')
    partitions = list()
    for line in lines:
        if line != None and line != '' and '0x5' not in line and 'NR' not in line:
            lineParts = line.split(' ')
            lineParts = [s for s in lineParts if s != '']
            partitions.append(
                device + lineParts[0] + ' ' + ',Start: ' + lineParts[1] + ',End: ' + lineParts[2] + ',Size: ' +
                (lineParts[3].replace(',', '.')) + ', Type:' + lineParts[4])
    return partitions


def GetLocales():
    output = runCommand(['sudo', 'localectl', 'list-locales'])
    return output.split('\n')


def GetKeymaps():
    output = runCommand(['sudo', 'localectl', 'list-keymaps'])
    return output.split('\n')


def CheckPostDeviceSelection(data):
    messages = Messages()
    if (data['device'] != None and data['device'] != ''):
        print 'true'
        return {'result': True}
    else:
        print 'falses'
        return {'result': False, 'message': messages[29]}


def NullCheckOnList(theList, requiredKeys):
    valid = True
    for key, value in theList.iteritems():
        if key in requiredKeys and (value == None or value == ''):
            valid = False
            break
    return valid


def CheckPostPartitionSelection(data):
    messages = Messages()
    valid = NullCheckOnList(data, ['rootPartition'])
    valid1 = data['rootPartition'] != data['swapPartition'] and data['rootPartition'] != data['homePartition']
    result = valid and valid1
    return {'result': result,
            'message': messages[30]}


def CheckPostUserDetails(data):
    messages = Messages()
    valid = NullCheckOnList(data, ['fullName', 'username', 'password', 'retypePassword', 'computerName'])
    valid1 = data['password'] == data['retypePassword']
    return {'result': valid and valid1,
            'message': messages[31]}


def CheckPostRootPasswords(data):
    messages = Messages()
    valid = NullCheckOnList(data, ['password', 'retypePassword'])
    valid1 = data['password'] == data['retypePassword']
    return {'result': valid and valid1, 'message': messages[32]}

def RunInstallation(data, logWindow):
    file = open('/tmp/install-properties', 'w')
    for key, value in data.iteritems():
        file.write(key + '="' + value + '"\n')
    file.close()
    WorkerThread(logWindow)

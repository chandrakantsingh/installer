import wx
import sys

sys.path.append('.')
import CardPanel as cp
import functions as f


class RootPasswordPanel(wx.Panel):
    def __init__(self, parent, messages):
        wx.Panel.__init__(self, parent, -1)
        self.messages = messages
        self.Sizer = wx.GridBagSizer()
        self.SetSizer(self.Sizer)
        self.Password = wx.TextCtrl(self, style=wx.TE_PASSWORD)
        self.RetypePassword = wx.TextCtrl(self, style=wx.TE_PASSWORD)
        self.AddWidgets()

    def AddWidgets(self):
        f.AddHeadingAndDescription(self, self.GetSizer(), self.messages[18], self.messages[19])
        self.Sizer.Add(wx.StaticText(self, -1, self.messages[20]), (2, 0), (1, 1),
                       wx.EXPAND | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_LEFT | wx.RIGHT | wx.LEFT, 20)
        self.Sizer.Add(self.Password, (3, 0), (1, 1),
                       wx.EXPAND | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_LEFT | wx.RIGHT | wx.LEFT, 20)
        self.Sizer.Add(wx.StaticText(self, -1, self.messages[21]), (4, 0), (1, 1),
                       wx.EXPAND | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_LEFT | wx.RIGHT | wx.LEFT, 20)
        self.Sizer.Add(self.RetypePassword, (5, 0), (1, 1),
                       wx.EXPAND | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_LEFT | wx.RIGHT | wx.LEFT, 20)
        self.Sizer.AddGrowableCol(0)

    def GetData(self):
        return {'password': self.Password.GetValue(), 'retypePassword': self.RetypePassword.GetValue()}

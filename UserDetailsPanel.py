import wx
import sys

sys.path.append('.')
import CardPanel as cp
import functions as f


class UserDetailsPanel(wx.Panel):
    def __init__(self, parent, messages):
        wx.Panel.__init__(self, parent, -1)
        self.messages = messages
        self.Sizer = wx.GridBagSizer()
        self.SetSizer(self.Sizer)
        self.FullName = wx.TextCtrl(self, -1)
        self.ComputerName = wx.TextCtrl(self, -1)
        self.Username = wx.TextCtrl(self, -1)
        self.Password = wx.TextCtrl(self, style=wx.TE_PASSWORD)
        self.RetypePassword = wx.TextCtrl(self, style=wx.TE_PASSWORD)

        self.Label1 = wx.StaticText(self, -1, self.messages[13])
        self.Label2 = wx.StaticText(self, -1, self.messages[14])
        self.Label3 = wx.StaticText(self, -1, self.messages[15])
        self.Label4 = wx.StaticText(self, -1, self.messages[16])
        self.Label5 = wx.StaticText(self, -1, self.messages[17])

        self.AddWidgets()

    def AddWidgets(self):
        f.AddHeadingAndDescription(self, self.GetSizer(), self.messages[11], self.messages[12])
        self.Sizer.Add(self.Label1, (2, 0), (1, 1), wx.EXPAND | wx.ALIGN_CENTER_VERTICAL | wx.LEFT | wx.RIGHT, 20)
        self.Sizer.Add(self.Label2, (4, 0), (1, 1), wx.EXPAND | wx.ALIGN_CENTER_VERTICAL | wx.LEFT | wx.RIGHT, 20)
        self.Sizer.Add(self.Label3, (6, 0), (1, 1), wx.EXPAND | wx.ALIGN_CENTER_VERTICAL | wx.LEFT | wx.RIGHT, 20)
        self.Sizer.Add(self.Label4, (8, 0), (1, 1), wx.EXPAND | wx.ALIGN_CENTER_VERTICAL | wx.LEFT | wx.RIGHT, 20)
        self.Sizer.Add(self.Label5, (10, 0), (1, 1), wx.EXPAND | wx.ALIGN_CENTER_VERTICAL | wx.LEFT | wx.RIGHT, 20)

        self.Sizer.Add(self.FullName, (3, 0), (1, 1), wx.EXPAND | wx.ALIGN_CENTER_VERTICAL | wx.LEFT | wx.RIGHT, 20)
        self.Sizer.Add(self.ComputerName, (5, 0), (1, 1), wx.EXPAND | wx.ALIGN_CENTER_VERTICAL | wx.LEFT | wx.RIGHT, 20)
        self.Sizer.Add(self.Username, (7, 0), (1, 1), wx.EXPAND | wx.ALIGN_CENTER_VERTICAL | wx.LEFT | wx.RIGHT, 20)
        self.Sizer.Add(self.Password, (9, 0), (1, 1), wx.EXPAND | wx.ALIGN_CENTER_VERTICAL | wx.LEFT | wx.RIGHT, 20)
        self.Sizer.Add(self.RetypePassword, (11, 0), (1, 1), wx.EXPAND | wx.ALIGN_CENTER_VERTICAL | wx.LEFT | wx.RIGHT,
                       20)

        self.Sizer.AddGrowableCol(0)

    def GetData(self):
        return {'fullName': self.FullName.GetValue(),
                'username': self.Username.GetValue(),
                'password': self.Password.GetValue(),
                'retypePassword': self.RetypePassword.GetValue(),
                'computerName': self.ComputerName.GetValue()}

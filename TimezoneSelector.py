import wx
import sys

sys.path.append('.')

import functions as f


class TimezoneSelector(wx.Panel):
    def __init__(self, parent, messages):
        wx.Panel.__init__(self, parent, -1)
        self.messages = messages
        self.SetSizer(wx.GridBagSizer())
        self.Timezone = wx.ListBox(self, -1, size=(-1, -1))
        self.InitTimezone()
        self.AddWidgets()

    def InitTimezone(self):
        langlist = []
        f = open('/opt/installer/zone.csv')
        lines = f.read().split('\n')
        for line in lines:
            words = line.split("\"")
            if len(words) >= 5:
                langlist.append(words[5])
        langlist.sort()
        for lang in langlist:
            self.Timezone.Append(lang)
        f.close()

    def AddWidgets(self):
        f.AddHeadingAndDescription(self, self.GetSizer(), self.messages[27], self.messages[28])
        self.GetSizer().Add(self.Timezone, (2, 0), (1, 1), wx.EXPAND | wx.ALL, 5)
        self.GetSizer().AddGrowableCol(0)
        self.GetSizer().AddGrowableRow(2)

    def GetData(self):
        return {'timezone': self.Timezone.GetStringSelection()}

#!/usr/bin/env python

import sys
import subprocess

def runCommand(line, success, failure):
	command = line.split(' ')
	p = subprocess.Popen(command, stdout=subprocess.PIPE, bufsize=1)
	for line in iter(p.stdout.readline, b''):
		print line,
	p.stdout.close()
	p.wait()
	if p.returncode != 0:
		print '\n' + failure + '\n'
		exit()
	if success != '':
		print '\n' + success + '\n'

def runCommandAndCaptureOutput(line, failure):
	command = line.split(' ')
	p = subprocess.Popen(command, stdout=subprocess.PIPE, bufsize=1)
	output = ''
	for line in iter(p.stdout.readline, b''):
		output = output + line
	p.stdout.close()
	p.wait()
	if p.returncode != 0:
		print '\n' + failure + '\n'
		exit()
	return output

if sys.argv[1] == 'build':
	print 'Nothing to build. Run sudo python setup.py install'

if sys.argv[1] == 'install':
	print 'Installing the installer into /opt'
	username = runCommandAndCaptureOutput('whoami', '').strip()
	runCommand('sudo cp -rvf ../installer /opt', '', 'Could not copy files.')
	runCommand('cp -vf installer.desktop /home/' + username + '/Desktop/installer.desktop', '', '')
	runCommand('chmod a+x /home/' + username + '/Desktop/installer.desktop', 'Installation complete.', 'Could not copy files.')
	runCommand('sudo rm -r /opt/installer/.git', '', '')
	runCommand('sudo rm -r /opt/installer/.idea', '', '')


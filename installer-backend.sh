#!/bin/bash

set -e
set +h

# Include the file that contains the installation parameters
. /tmp/install-properties

echo "Installing with the following parameters:"
echo ""
echo "ROOT_PARTITION : $ROOT_PART"
echo "SWAP_PARTITION : $SWAP_PART"
echo "HOME_PARTITION : $HOME_PART"

# Check if we are running as root. Exit if we are not running as root
if [[ $EUID -ne 0 ]]; then
  echo "You must be a root user" 2>&1
  exit 1
fi

# The source and destination variable declarations
export SRC="/mnt/src"
export DEST="/mnt/dest"

# Create the source and destination directories if not already present.
echo "Source and destination directories present. Was there a failed installation attempt?"

#if [ -d $SRC ]
#then
#	echo "$SRC present. Removing..."
#	set +e
#	umount -v $SRC
#	set -e
#	rm -rv $SRC
#fi

if [ -d $DEST ]
then
	echo "$DEST present. Removing..."
	set +e
	umount -v $DEST/dev/pts
	umount -v $DEST/dev/shm
	umount -v $DEST/dev
	umount -v $DEST/sys
	umount -v $DEST/proc
	umount -v $DEST/run
	umount -v $DEST/boot/efi
	umount -v $DEST
	set -e
	rm -rv $DEST
fi

echo "Creating source and destination directories..."
# mkdir -pv $SRC
mkdir -pv $DEST

# Mount the squashed file system for copying
# echo "Mounting the root filesystem..."
# mount -v -o ro,loop "`find /mnt/.boot -name root.sfs`" $SRC

# Format the destination partition and mount it. Mount /home if specified.
echo "Formatting root partition..."
set +e
umount $ROOT_PART
set -e
mkfs.ext4 -v $ROOT_PART

mount $ROOT_PART $DEST
if [ "$HOME_PART" != "-" ]
then
	mkdir -pv $DEST/home
	mount $HOME_PART $DEST/home
fi

ROOT_PART_BY_UUID=`sudo blkid $ROOT_PART | cut '-d"' -f2`

if [ "$SWAP_PART" != "-" ]
then

SWAP_PART_BY_UUID=`sudo blkid $SWAP_PART | cut '-d"' -f2`
cat >> /tmp/install-properties << EOF
SWAP_PART_BY_UUID="$SWAP_PART_BY_UUID"
EOF

fi

if [ "$HOME_PART" != "-" ]
then

HOME_PART_BY_UUID=`sudo blkid $HOME_PART | cut '-d"' -f2`
cat >> /tmp/install-properties << EOF
HOME_PART_BY_UUID="$HOME_PART_BY_UUID"
EOF

fi

cat >> /tmp/install-properties << EOF
ROOT_PART_BY_UUID="$ROOT_PART_BY_UUID"
EOF

# Copy the files and display each file as its being copied.
unsquashfs -f -d $DEST /mnt/.boot/medium/aryalinux/root.sfs
# cp -prvf $SRC/* $DEST/

# Copy the post-install.sh script and install-properties to the root of destination
cp /opt/installer/post-install.sh $DEST/
cp /tmp/install-properties $DEST/
chmod a+x $DEST/post-install.sh

# Mount the virtual file systems
mount -v --bind /dev $DEST/dev
mount -vt devpts devpts $DEST/dev/pts -o gid=5,mode=620
mount -vt proc proc $DEST/proc
mount -vt sysfs sysfs $DEST/sys
mount -vt tmpfs tmpfs $DEST/run

if [ -h $DEST/dev/shm ]; then
  mkdir -pv $DEST/$(readlink $DEST/dev/shm)
fi

# Enter the chroot environment to run post-install.sh
chroot "$DEST" /usr/bin/env -i              \
    HOME=/root TERM="$TERM" PS1='\u:\w\$ ' \
    PATH=/bin:/usr/bin:/sbin:/usr/sbin     \
    /bin/bash /post-install.sh &> /tmp/post-install.log

# Cleaning up.
rm $DEST/post-install.sh
rm -rvf /tmp/install-properties
rm -rvf $DEST/install-properties
rm -rvf $DEST/opt/installer
# rm -rvf $DEST/opt/LanguageSelector
rm -rvf `find $DEST/etc/xdg/autostart/ -name languageselector.desktop`

set +e

# umount $SRC
umount $DEST/dev/pts
umount $DEST/dev
umount $DEST/proc
umount $DEST/sys
umount $DEST/run
umount $DEST
umount $DEST/boot/efi
swapoff $SWAP_PART

set -e

echo "System transfer complete."

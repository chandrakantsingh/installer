About AryaLinux Installer
-------------------------
The AryaLinux Installer is a very simple Python based installer. The UI
is developed using wxPython and it uses a bash script in the background
to do the actual work of installation. THIS PROJECT IS STILL UNDER HEAVY
DEVELOPMENT AND IS FAR FROM COMPLETE.

How it works
------------
There are two parts to the installer:
- The UI
- The backend scripts

The UI
------
The UI is written in python and wxPython provides the necessary UI widgets. So python and wxPython are the dependencies
of the UI. The UI presents few screens where the user can provide installation information like target partition name,
user details, locale, region and keyboard details etc. These inputs are preserved in a file - /tmp/install-properties
which is then used by the backend scripts

The backend scripts

There are two backend scripts:
    - installer-backend.sh
    - post-install.sh
    

installer-backend.sh
--------------------
This script simply searches for the file root.sfs in /mnt/.boot, mounts it and copies files from it to the target
partition. Before copying, the target partition is formatted using the ext4 filesystem. Once copying completes,
the script enters chroot environment and executes post-install.sh

post-install.sh
---------------
This script does the post installation tasks like user creation, adding the user to the right groups, cleaning up
files etc. This file also sets up the locale, languge and keyboard settings and the /etc/fstab file.

Improvement
-----------
A lot of improvement can be done to turn this into a full blown installer
like the one that usually comes in most linux distributions. A lot of things
can be moved into configuration files so that the the installer can be
customized without changing source code. This is something that I would
be working on in near future.

Translations
------------
If you want the installer in any other language, please help us in translation
of the messages in the installer. The messages in the installer are present in
the messages directory. Each file in this directory contains messages in one
language. Each line contains a different message that appears in some part of
the installer. If you want to submit a translation, please translate this file
and save it under the same name as the language it represents and the email the
file to us at aryalinux11@gmail.com and we would include it in the langauge list.

Thanks in advance for your support. For further queries/suggestions, please
mail us at aryalinux11@gmail.com

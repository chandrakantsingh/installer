import wx
import sys

sys.path.append('.')
import CardPanel as cp
import functions as f


class DeviceSelectionPanel(wx.Panel):
    def __init__(self, parent, messages):
        wx.Panel.__init__(self, parent, -1)
        self.messages = messages
        self.Sizer = wx.GridBagSizer()
        self.SetSizer(self.Sizer)
        self.Devices = wx.ComboBox(self, -1, style=wx.CB_READONLY)
        self.AddWidgets()

    def AddWidgets(self):
        f.AddHeadingAndDescription(self, self.GetSizer(), self.messages[4], self.messages[5])
        self.Sizer.Add(self.Devices, (2, 0), (1, 1), wx.EXPAND | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_LEFT | wx.ALL, 10)
        self.Sizer.Add(wx.StaticText(self, -1, ""), (3, 0), (1, 1))
        self.Sizer.AddGrowableCol(0)
        self.Sizer.AddGrowableRow(3)
        devices = f.getDevices()
        for device in devices:
            self.Devices.Append(device)
        self.Devices.SetSelection(0)

    def GetData(self):
        selection = self.Devices.GetValue()
        if selection != None and selection != '':
            parts = selection.split(',')
            device = parts[0]
            return {'device': device}
        else:
            return {'device': None}

import wx
import sys

sys.path.append('.')
import CardPanel as cp
import functions as f


class PartitionSelectionPanel(wx.Panel):
    def __init__(self, parent, messages):
        wx.Panel.__init__(self, parent, -1)
        self.DeviceName = None
        self.messages = messages
        self.Sizer = wx.GridBagSizer()
        self.SetSizer(self.Sizer)
        self.RootPartition = wx.ComboBox(self, -1)
        self.HomePartition = wx.ComboBox(self, -1)
        self.SwapPartition = wx.ComboBox(self, -1)
        self.AddWidgets()

    def AddWidgets(self):
        f.AddHeadingAndDescription(self, self.GetSizer(), self.messages[6], self.messages[7])
        self.Sizer.Add(wx.StaticText(self, -1, self.messages[8]), (2, 0), (1, 1),
                       wx.EXPAND | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_LEFT | wx.ALL, 5)
        self.Sizer.Add(self.RootPartition, (3, 0), (1, 1),
                       wx.EXPAND | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_LEFT | wx.ALL, 5)
        self.Sizer.Add(wx.StaticText(self, -1, self.messages[9]), (4, 0), (1, 1),
                       wx.EXPAND | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_LEFT | wx.ALL, 5)
        self.Sizer.Add(self.HomePartition, (5, 0), (1, 1),
                       wx.EXPAND | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_LEFT | wx.ALL, 5)
        self.Sizer.Add(wx.StaticText(self, -1, self.messages[10]), (6, 0), (1, 1),
                       wx.EXPAND | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_LEFT | wx.ALL, 5)
        self.Sizer.Add(self.SwapPartition, (7, 0), (1, 1),
                       wx.EXPAND | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_LEFT | wx.ALL, 5)
        self.Sizer.Add(wx.StaticText(self, -1, ""), (8, 0), (1, 1))
        self.Sizer.AddGrowableCol(0)
        self.Sizer.AddGrowableRow(8)

    def SetDeviceName(self, device):
        self.DeviceName = device
        self.RootPartition.Clear()
        self.RootPartition.Clear()
        self.RootPartition.Clear()
        partitions = f.getPartitions(device)
        self.HomePartition.Append("-")
        self.SwapPartition.Append("-")
        self.HomePartition.SetStringSelection("-")
        self.SwapPartition.SetStringSelection("-")
        for partition in partitions:
            if '0x82' in partition or '0657fd6d-a4ab-43c4-84e5-0933c84b4f4f' in partition:
                self.SwapPartition.Append(partition)
            else:
                print partition
                self.RootPartition.Append(partition)
                self.HomePartition.Append(partition)

    def GetData(self):
        rootPartition = self.RootPartition.GetValue().split(' ')[0]
        if self.HomePartition.GetValue() != '-':
            homePartition = self.HomePartition.GetValue().split(' ')[0]
        else:
            homePartition = '-'
        if self.SwapPartition.GetValue() != '-':
            swapPartition = self.SwapPartition.GetValue().split(' ')[0]
        else:
            swapPartition = '-'

        return {'device': self.DeviceName, 'rootPartition': rootPartition, 'homePartition': homePartition,
                'swapPartition': swapPartition}

import wx


class InstallationLogPanel(wx.Panel):
    def __init__(self, parent, messages):
        wx.Panel.__init__(self, parent, -1)
        self.messages = messages
        self.LogWindow = wx.TextCtrl(self, -1, style=wx.TE_MULTILINE | wx.TE_READONLY)
        self.AddWidgets()

    def AddWidgets(self):
        self.SetSizer(wx.GridBagSizer())
        self.GetSizer().Add(wx.StaticText(self, -1, self.messages[26]), (0, 0), (1, 1), wx.EXPAND | wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)
        self.GetSizer().Add(self.LogWindow, (1, 0), (1, 1), wx.EXPAND | wx.ALL, 5)
        self.GetSizer().AddGrowableRow(1)
        self.GetSizer().AddGrowableCol(0)

    def GetLogWindow(self):
        return self.LogWindow

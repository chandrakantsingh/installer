import wx
from threading import *
import time
import subprocess

ID_START = wx.NewId()
ID_STOP = wx.NewId()

EVT_RESULT_ID = wx.NewId()


def EVT_RESULT(win, func):
    win.Connect(-1, -1, EVT_RESULT_ID, func)


class ResultEvent(wx.PyEvent):
    """Simple event to carry arbitrary result data."""

    def __init__(self, data):
        """Init Result Event."""
        wx.PyEvent.__init__(self)
        self.SetEventType(EVT_RESULT_ID)
        self.data = data


class WorkerThread(Thread):
    def __init__(self, notifyWindow):
        Thread.__init__(self)
        self.NotifyWin = notifyWindow
        self.start()

    def run(self):
        p = subprocess.Popen(['sudo', '/opt/installer/installer-backend.sh'], stdout=subprocess.PIPE, bufsize=1)
        for line in iter(p.stdout.readline, b''):
            wx.PostEvent(self.NotifyWin, ResultEvent(line))
        wx.PostEvent(self.NotifyWin, ResultEvent(None))

import wx
import sys

sys.path.append('.')
import functions as f


class LocaleAndKeyboardSelectorPanel(wx.Panel):
    def __init__(self, parent, messages):
        wx.Panel.__init__(self, parent, -1)
        self.messages = messages
        self.Sizer = wx.GridBagSizer()
        self.SetSizer(self.Sizer)
        self.AddWidgets()
        self.GetSizer().AddGrowableCol(0)

    def AddWidgets(self):
        self.Label1 = wx.StaticText(self, -1, self.messages[24])
        self.Label2 = wx.StaticText(self, -1, self.messages[25])
        self.LocaleComboBox = wx.ComboBox(self, -1, style=wx.CB_READONLY)
        self.KeymapComboBox = wx.ComboBox(self, -1, style=wx.CB_READONLY)
        self.Sizer.Add(self.Label1, (2, 0), (1, 1), wx.EXPAND | wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)
        self.Sizer.Add(self.Label2, (4, 0), (1, 1), wx.EXPAND | wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)
        self.Sizer.Add(self.LocaleComboBox, (3, 0), (1, 1), wx.EXPAND | wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)
        self.Sizer.Add(self.KeymapComboBox, (5, 0), (1, 1), wx.EXPAND | wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)
        self.InitializeWidgets()

    def InitializeWidgets(self):
        f.AddHeadingAndDescription(self, self.Sizer, self.messages[22], self.messages[23])
        locales = f.GetInstalledLocales()
        for locale in locales:
            self.LocaleComboBox.Append(locale)
        keymaps = f.GetInstalledKeymaps()
        for keymap in keymaps:
            self.KeymapComboBox.Append(keymap)

import wx
from time import sleep
import os
 
class SlideshowPanel(wx.Panel):
	def __init__(self, imageDir, timeBetweenSlides):
		wx.Panel.__init__(self, -1)
		self.sleepTime = timeBetweenSlides
		self.SetSizer(wx.BoxSizer(wx.HORIZONTAL))
		self.imgCtrl = wx.StaticBitmap(self.panel, -1, wx.BitmapFromImage(img))
		self.GetSizer().Add(self.imgCtrl, 0, wx.ALL | wx.EXPAND, 5)

		self.images = os.listdir(imageDir)
		for i in range(0, len(self.images)):
			self.images[i] = imageDir + self.images[i]

		img = wx.EmptyImage(500, 300)
		self.imgCtrl.SetBitmap(wx.BitmapFromImage(img))
		self.Refresh()

	def SetImage(self, path):
		img = wx.Image(path, wx.BITMAP_TYPE_ANY)
		self.imgCtrl.SetBitmap(wx.BitmapFromImage(img))
		self.panel.Refresh()

	def ScrollImages(self):
		for i in range(0, len(self.images)):
			wx.CallAfter(self.SetImage, self.images[i])
			sleep(self.sleepTime)

	def Start(self):
		thread.start_new_thread( self.ScrollImages, (), )


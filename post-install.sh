#!/bin/bash

set -e
set +h

. /install-properties

# Generate the initial ramdisk
INITRD=`ls /boot/initrd*`
/usr/bin/dracut -f $INITRD `ls /lib/modules/`

# Generating the /etc/fstab file here
cat > /etc/fstab <<EOF
# Begin /etc/fstab

# file system  mount-point  type     options             dump  fsck
#                                                              order

UUID=$ROOT_PART_BY_UUID     /            ext4     defaults            1     1
EOF

# If /home is specified then add the same
if [ "$HOME_PART" != "-" ]
then

cat >> /etc/fstab <<EOF
UUID=$HOME_PART_BY_UUID     /home        ext4     defaults            1     1
EOF

fi

# If swap partition is specified then add the same
if [ "$SWAP_PART" != "-" ]
then

cat >> /etc/fstab <<EOF
UUID=$SWAP_PART_BY_UUID     swap         swap     pri=1               0     0
EOF

fi

# Installing grub
if [ -d /sys/firmware/efi ]
then

EFIPART="$DEVICE`partx -s -o NR,START,END,SECTORS,SIZE,NAME,TYPE $DEVICE | tr -s ' ' | grep "c12a7328-f81f-11d2-ba4b-00a0c93ec93b" | sed "s@^ *@@g" | cut "-d " -f1`"
EFIPART_UUID=`blkid $EFIPART | cut '-d"' -f4`

if [ "x$EFIPART" == "x$DEVICE" ]
then

/sbin/grub-install "$DEVICE"

else

mkdir -pv /boot/efi

{
set +e
mount -vt vfat $EFIPART /boot/efi
mount -vt efivarfs efivars /sys/firmware/efi/efivars
set -e
}

cat >> /etc/fstab <<EOF
UUID=$EFIPART_UUID       /boot/efi    vfat     defaults            0     1
efivarfs       /sys/firmware/efi/efivars  efivarfs  defaults  0      1
EOF

BOOTLOADER_ID="AryaLinux 1.0 Aranya ($ROOT_PART)"
PARTNUMBER=`echo $EFIPART | sed "s@$DEVICE@@g"`

grub-install --target=`uname -m`-efi --efi-directory=/boot/efi --bootloader-id="$BOOTLOADER_ID" --recheck --debug
# efibootmgr --create --gpt --disk $DEVICE --part $PARTNUMBER --write-signature --label "$OS_NAME $OS_VERSION $OS_CODENAME" --loader "/EFI/$BOOTLOADER_ID/grubx64.efi"

fi

else

/sbin/grub-install "$DEVICE"

fi

# End the /etc/fstab file with a comment
cat >> /etc/fstab <<EOF

# End /etc/fstab
EOF


# Setting up the Machine ID
/bin/systemd-machine-id-setup

# Name this system
echo "$COMPUTER_NAME" > /etc/hostname

# Deleting the aryalinux live user
/usr/sbin/userdel -r aryalinux

# Adding user and changing password
/usr/sbin/useradd -m -k /etc/skel -c "$FULL_NAME" $USERNAME
/usr/bin/yes "$PASSWORD" | /bin/passwd $USERNAME
/usr/sbin/usermod -a -G audio $USERNAME
/usr/sbin/usermod -a -G video $USERNAME
/usr/sbin/usermod -a -G wheel $USERNAME
/usr/sbin/usermod -a -G lpadmin $USERNAME

# Preparing user's home directory:
cd /home/$USERNAME
su - "$USERNAME" -c "xdg-user-dirs-update --force"

# Changing the root password
/usr/bin/yes "$ROOT_PASSWORD" | passwd root


# Generating grub.conf
if [ "x$SWAP_PART" != "x-" ]
then
	sed -i "s@quiet splash@quiet splash resume=/dev/disk/by-uuid/$SWAP_PART_BY_UUID@g" /etc/default/grub
fi

set +e
/sbin/grub-mkconfig -o /boot/grub/grub.cfg
set -e

# Specify the locale in locale.conf. This is used for the desktop environments.
echo "LANG=$LOCALE" > /etc/locale.conf
echo "LANGUAGE=$LOCALE" >> /etc/locale.conf
echo "LC_ALL=$LOCALE" >> /etc/locale.conf

# Specify the language for i18n
echo "export LANG=$LOCALE" > /etc/profile.d/i18n.sh
echo "export LANGUAGE=$LOCALE" >> /etc/profile.d/i18n.sh
echo "export LC_ALL=$LOCALE" >> /etc/profile.d/i18n.sh

# Configure the timezone
ln -sfv "/usr/share/zoneinfo/$TIMEZONE" /etc/localtime

# Specify the language and keyboard layout
cat > /etc/vconsole.conf <<EOF
KEYMAP="$KEYMAP"
EOF

sed -i "s@autologin-user=@#autologin-user=@g" $LFS/etc/lightdm/lightdm.conf
sed -i "s@autologin-user-timeout=0@#autologin-user-timeout=0@g" $LFS/etc/lightdm/lightdm.conf
sed -i "s@pam-service=lightdm-autologin@#pam-service=lightdm-autologin@g" $LFS/etc/lightdm/lightdm.conf

# Create alps source dir
sudo mkdir -pv /var/cache/alps/sources/
sudo chmod a+rw /var/cache/alps/sources

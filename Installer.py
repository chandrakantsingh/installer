import wx
import sys

sys.path.append('.')
import CardPanel as cp
import DeviceSelectionPanel as dsp
import PartitionSelectionPanel as psp
import UserDetailsPanel as udp
import RootPasswordPanel as rpp
import KeyboardAndLocaleSelectionPanel as kls
import InstallationLogPanel as ilp
import TimezoneSelector as tzs
import functions as f
import subprocess
import time
from threading import *
from WorkerThread import *


class MainFrame(wx.Frame):
    def __init__(self):
        wx.Frame.__init__(self, None, -1, style=wx.SYSTEM_MENU | wx.CAPTION | wx.CLOSE_BOX | wx.CLIP_CHILDREN)

        jpg = wx.Image('/opt/installer/header.jpg', wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        header = wx.StaticBitmap(self, -1, jpg, (10, 5), (jpg.GetWidth(), jpg.GetHeight()))

        messages = f.Messages()
        self.SetTitle(messages[3])

        self.names = ["deviceSelection", "partitionSelection", "userDetails", "rootPassword",
                      "keyboardAndLocaleSelector", 'timezoneSelector', 'logWindow']
        self.validators = [f.CheckPostDeviceSelection, f.CheckPostPartitionSelection, f.CheckPostUserDetails,
                           f.CheckPostRootPasswords, None, None, None]
        self.index = 0
        self.cardPanel = cp.CardPanel(self)

        self.DeviceSelectionPanel = dsp.DeviceSelectionPanel(self.cardPanel, messages)
        self.PartitionSelectionPanel = psp.PartitionSelectionPanel(self.cardPanel, messages)
        self.UserDetailsPanel = udp.UserDetailsPanel(self.cardPanel, messages)
        self.RootPasswordPanel = rpp.RootPasswordPanel(self.cardPanel, messages)
        self.KeyboardAndLocaleSelector = kls.KeyboardAndLocaleSelectionPanel(self.cardPanel, messages)
        self.InstallationLogPanel = ilp.InstallationLogPanel(self.cardPanel, messages)
        self.TimezoneSelector = tzs.TimezoneSelector(self.cardPanel, messages)

        self.Screens = [self.DeviceSelectionPanel, self.PartitionSelectionPanel, self.UserDetailsPanel,
                        self.RootPasswordPanel, self.KeyboardAndLocaleSelector, self.TimezoneSelector]

        self.cardPanel.AddScreen(self.DeviceSelectionPanel, "deviceSelection")
        self.cardPanel.AddScreen(self.PartitionSelectionPanel, "partitionSelection")
        self.cardPanel.AddScreen(self.UserDetailsPanel, "userDetails")
        self.cardPanel.AddScreen(self.RootPasswordPanel, "rootPassword")
        self.cardPanel.AddScreen(self.KeyboardAndLocaleSelector, 'keyboardAndLocaleSelector')
        self.cardPanel.AddScreen(self.TimezoneSelector, 'timezoneSelector')
        self.cardPanel.AddScreen(self.InstallationLogPanel, 'logWindow')

        self.SetSizer(wx.GridBagSizer())
        self.GetSizer().Add(header, (0, 0), (1, 1), wx.ALL | wx.EXPAND, 0)
        self.GetSizer().Add(self.cardPanel, (1, 0), (1, 1), wx.ALL | wx.EXPAND, 10)
        self.GetSizer().AddGrowableRow(1)
        self.GetSizer().AddGrowableCol(0)

        self.buttonPanel = wx.Panel(self, -1)
        self.buttonPanel.SetSizer(wx.GridSizer(1, 0, 5, 5))
        self.prev = wx.Button(self.buttonPanel, -1, messages[0])
        self.next = wx.Button(self.buttonPanel, -1, messages[1])
        self.cancel = wx.Button(self.buttonPanel, -1, messages[2])
        self.buttonPanel.GetSizer().Add(self.prev)
        self.buttonPanel.GetSizer().Add(self.next)
        self.buttonPanel.GetSizer().Add(self.cancel)
        self.GetSizer().Add(self.buttonPanel, (2, 0), (1, 1), wx.ALIGN_RIGHT | wx.RIGHT | wx.BOTTOM, 10);

        self.cardPanel.SwitchScreen(self.names[self.index])

        self.next.Bind(wx.EVT_BUTTON, self.OnNext, self.next)
        self.prev.Bind(wx.EVT_BUTTON, self.OnPrevious, self.prev)
        self.cancel.Bind(wx.EVT_BUTTON, self.OnCancel, self.cancel)
        EVT_RESULT(self, self.OnResult)

        # And indicate we don't have a worker thread yet
        self.worker = None

    def OnResult(self, event):
        messages = f.Messages()
        if event.data != None:
            self.InstallationLogPanel.LogWindow.AppendText(event.data)
        else:
            dlg = wx.MessageDialog(self, messages[37], messages[38], wx.ICON_INFORMATION)
            dlg.ShowModal()
            exit(0)

    def OnNext(self, event):
        messages = f.Messages()
        #validator = self.validators[self.index]
        screen = self.Screens[self.index]
        data = screen.GetData()
        #if validator != None and not validator(data)['result']:
        #    self.Warn(self, validator(data)['message'])
        #    return
        self.index = self.index + 1
        if self.index == 1:
            device = self.DeviceSelectionPanel.GetData()['device']
            self.PartitionSelectionPanel.SetDeviceName(device)
        if self.index == len(self.names) - 1:
            dlg = wx.MessageDialog(self, messages[35], messages[36], wx.YES_NO | wx.ICON_QUESTION)
            result = dlg.ShowModal()
            if result == wx.ID_YES:
                partitionData = self.PartitionSelectionPanel.GetData()
                userData = self.UserDetailsPanel.GetData()
                rootData = self.RootPasswordPanel.GetData()
                localeData = self.KeyboardAndLocaleSelector.GetData()
                tzData = self.TimezoneSelector.GetData()
                data = {'DEVICE': partitionData['device'], 'ROOT_PART': partitionData['rootPartition'],
                        'HOME_PART': partitionData['homePartition'], 'SWAP_PART': partitionData['swapPartition'],
                        'FULL_NAME': userData['fullName'], 'USERNAME': userData['username'],
                        'PASSWORD': userData['password'], 'COMPUTER_NAME': userData['computerName'],
                        'ROOT_PASSWORD': rootData['password'], 'LOCALE': localeData['locale'],
                        'KEYMAP': localeData['keymap'], 'TIMEZONE': tzData['timezone']}
                file = open('/tmp/install-properties', 'w')
                for key, value in data.iteritems():
                    file.write(key + '="' + value + '"\n')
                file.close()
                self.index = len(self.names) - 1
                self.cardPanel.SwitchScreen(self.names[self.index])
                self.cancel.Disable()
                f.RunInstallation(data, self)
                self.prev.Disable()
                self.next.Disable()
                return
            else:
                self.index = self.index - 1
                return
            self.index = len(self.names) - 1
        self.cardPanel.SwitchScreen(self.names[self.index])

    def OnPrevious(self, event):
        self.index = self.index - 1
        if self.index < 0:
            self.index = 0
        self.cardPanel.SwitchScreen(self.names[self.index])

    def OnCancel(self, event):
        exit(0)

    def Warn(self, parent, message, caption='Error!'):
        messages = f.Messages()
        dlg = wx.MessageDialog(parent, message, messages[34], wx.OK | wx.ICON_ERROR)
        dlg.ShowModal()
        dlg.Destroy()


app = wx.App()
mf = MainFrame()
mf.SetSize((1000, 600))
mf.Center()
mf.Show()
app.MainLoop()

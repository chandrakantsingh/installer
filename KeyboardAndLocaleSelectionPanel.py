import wx
import sys

sys.path.append('.')
import functions as f


class KeyboardAndLocaleSelectionPanel(wx.Panel):
    def __init__(self, parent, messages):
        wx.Panel.__init__(self, parent, -1)
        self.messages = messages
        self.SetSizer(wx.GridBagSizer())
        self.AddWidgets()

    def AddWidgets(self):
        self.Label1 = wx.StaticText(self, -1, self.messages[24])
        self.Label2 = wx.StaticText(self, -1, self.messages[25])
        self.Locale = wx.ListBox(self, -1, size=(-1, -1))
        self.Keyboard = wx.ListBox(self, -1, size=(-1, -1))

        self.OtherSizer1 = wx.GridSizer(1, 2, 5, 5)
        self.OtherSizer2 = wx.GridSizer(1, 2, 5, 5)

        self.GetSizer().Add(self.OtherSizer1, (2, 0), (1, 1), wx.EXPAND | wx.ALL, 5)
        self.GetSizer().Add(self.OtherSizer2, (3, 0), (1, 1), wx.EXPAND | wx.ALL, 5)
        self.OtherSizer1.Add(self.Label1, 0, wx.EXPAND | wx.ALL, 5)
        self.OtherSizer1.Add(self.Label2, 0, wx.EXPAND | wx.ALL, 5)
        self.OtherSizer2.Add(self.Locale, 0, wx.EXPAND | wx.ALL, 5)
        self.OtherSizer2.Add(self.Keyboard, 0, wx.EXPAND | wx.ALL, 5)

        f.AddHeadingAndDescription(self, self.GetSizer(), self.messages[22], self.messages[23])

        locales = f.GetLocales()
        keymaps = f.GetKeymaps()

        for locale in locales:
            self.Locale.Append(locale)

        for keymap in keymaps:
            self.Keyboard.Append(keymap)

        self.GetSizer().AddGrowableCol(0)
        self.GetSizer().AddGrowableRow(3)

    def GetData(self):
        return {'keymap': self.Keyboard.GetStringSelection(), 'locale': self.Locale.GetStringSelection()}

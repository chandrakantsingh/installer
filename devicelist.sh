#!/bin/bash

set -e

lsblk | grep disk | grep -v "boot" | tr -s ' '
